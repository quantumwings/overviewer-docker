FROM ubuntu:16.04
COPY overviewer.gpg.asc ./
RUN \
    apt-get update \
    && apt-get -y install apt-transport-https \
    && echo 'deb https://overviewer.org/debian ./' >>  /etc/apt/sources.list \
    && apt-key add overviewer.gpg.asc \
    && apt-get update \
    && apt-get install -y minecraft-overviewer \
    && rm -rf /var/lib/apt/lists/*

RUN useradd -rm overviewer
USER overviewer

RUN \
    mkdir -p ~/.minecraft/versions/1.12/ \
    && python2 -c "import urllib; print urllib.urlopen('https://s3.amazonaws.com/Minecraft.Download/versions/1.12/1.12.jar').read()" > ~/.minecraft/versions/1.12/1.12.jar

ENTRYPOINT [ "overviewer.py" ]